# WebCollector
WebCollector is an open source web crawler framework based on Java.It provides
  some simple interfaces for crawling the Web,you can setup a
  multi-threaded web crawler in less than 5 minutes.




## 官网
[https://github.com/CrawlScript/WebCollector](https://github.com/CrawlScript/WebCollector)



## 通知

由于近期项目在Github更新比较勤快，麻烦各位在Github中访问最新版本，等版本稳定，再复制一份过来：
[https://github.com/CrawlScript/WebCollector](https://github.com/CrawlScript/WebCollector)
